# THE AYAB SOFTWARE ADAPTED FOR QUICK AND EASY RUN ON LINUX WITH PYTHON >=3.9 

## Usage

	python -m venv .venv
	source .venv/bin/activate
	pip install -r requirements.txt
	python run.py


## AYAB - All Yarns Are Beautiful

Original repo here: [https://github.com/AllYarnsAreBeautiful/ayab-desktop](https://github.com/AllYarnsAreBeautiful/ayab-desktop)
